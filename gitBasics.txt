*************GIT BASIC COMMANDS*******************

STAGING AND COMMITTING:

    1. To initialize a git repository (a .git folder is created at this step) -> first mandatory step
    >git init
    NOTE: cd into the correct project folder before doing this.

    2. Windows cmd command to view hidden files(to see the .git folder)
    >dir /a

    3. Windows cmd command to view contents of the .git folder.
    >dir .git

    4. Once changes are made to your local repository, 
    they should be moved to staging area to track your changes
    and it is the files in the staging area that is committed.

    untracked/unstaged files/changes----->staged files----->changes are committed 

    To check status at any of these steps:

    >git status

    5. To stage a file or multiple files by filename
    >git add <filename>
    >git add <filename1> <filename2>

    6. To add all unstaged files
    >git add .

    7. To unstage a staged file
    >git restore --staged <filename>

    8. To commit changes in staging area with a message
    >git commit -m "write your commit message here"

    9. To view all existing commits
    >git log

    10. To rollback or revert to a previous commit
    >git reset <commit_id>
    NOTE: commit_id is visible from the previous git command -> git log

    11. To create/ add a remote 
    >git remote add <remotename> <https address i.e. url>

    12. To delete a remote
    >git remote rm <remotename>

    13.view remotes 
    
    To view all remotes
    >git remote

    To view remotes with url
    >git remote -v

    14. To push local changes (to both remote and thus github)
    >git push <remotename> <branchname>
    NOTE: to force push to overwrite YOUR remote repo with YOUR local repo:
    >git push <remotename> <branchname> -f

GIT STASH

    1. Consider certain changes are been made 
    and after committing those changes certain new changes are made.
    But if you don not want to commit those changes yet 
    and have to work on something else from the previous commit, you can stash them 
    and later pop out these stashed code to your unstaged area by using:

    >git stash
    >git stash pop

    NOTE: to stash make use first all changes are staged using -> git add .
            
    2. To delete all that is in the stashed area, 
    if you do not want them to be poped later use:

    >git stash clear

GIT BRANCHES

    1. view existing branches
    >git branch
    NOTE: *for a branch shows that, current head is pointing to that branch.

    2. To view local and remote banches separately
    >git branch -r
    NOTE: remote branches are read only but you can ceckout those branches as well.
    All remote branches are prefixed with 'remote-repo/'

    2. create a new branch
    >git branch <branchname>

    3. checkout of a branch i.e. point HEAD to that branch
    >git checkout <branchname>

    4. To delete a branch
    >git branch -d <branchname> 
    NOTE: before deleting make sure to checkout to a different branch.




